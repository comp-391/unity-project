using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class EndText : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private GameObject canvas;

    private float _timerToMainMenu = 2.0f;

    private bool _startTimer = false;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player")) return;
        if (canvas == null) return;
        canvas.SetActive(true);
        _startTimer = true;
    }
    void Start()
    {
        if (canvas == null) Debug.Log("This will break!");
    }
    
    private void Update()
    {
        if (!_startTimer)
        {
            return;
        }

        _timerToMainMenu -= Time.deltaTime;
        if (_timerToMainMenu <= 0.0f)
        {
            _startTimer = false;
            SceneManager.LoadScene("MainMenu");
        }
    }
}
