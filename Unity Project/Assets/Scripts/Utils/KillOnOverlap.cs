﻿using System;
using Characters;
using UnityEngine;

namespace Utils
{
    public class KillOnOverlap : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.gameObject.CompareTag("Player")) return;
            CharacterHealth characterHealth = other.gameObject.GetComponent<CharacterHealth>();
            if (characterHealth == null) return;
            characterHealth.ReceiveHit();
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!other.gameObject.CompareTag("Player")) return;
            CharacterHealth characterHealth = other.gameObject.GetComponent<CharacterHealth>();
            if (characterHealth == null) return;
            characterHealth.ReceiveHit();
        }
    }
}