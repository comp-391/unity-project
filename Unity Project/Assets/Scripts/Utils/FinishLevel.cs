﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Utils
{
    public class FinishLevel : MonoBehaviour
    {
        [SerializeField]
        private String nextLevel = "New leve";
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Player")) return;
            SceneManager.LoadScene(nextLevel);
        }
    }
}