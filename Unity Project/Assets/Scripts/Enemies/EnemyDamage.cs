﻿using System;
using Characters;
using UnityEngine;

namespace Enemies
{
    public class EnemyDamage : MonoBehaviour
    {
        private void OnCollisionEnter2D(Collision2D col)
        {
            if (!col.gameObject.CompareTag("Player")) return;
            
            CharacterHealth characterHealth = col.gameObject.GetComponent<CharacterHealth>();
            if (characterHealth == null) return;
            characterHealth.ReceiveHit();
            
        }
    }
}