﻿using System;
using System.Timers;
using Characters;
using UnityEngine;

namespace Enemies
{
    [RequireComponent(typeof(CharacterHealth))]
    public class EnemyDeadComponent : MonoBehaviour
    {
        private bool _isDying = false;

        private float _dyingTimer = 0.0f;
        
        private void Start()
        {

        }

        private void Update()
        {
            if (!_isDying) return;
            _dyingTimer += Time.deltaTime;
            if (_dyingTimer >= 2.0f) Died();
        }

        public void OnDie()
        {
            Collider2D[] collider2Ds= GetComponents<Collider2D>();
            foreach (var col in collider2Ds)
            {
                col.enabled = false;
            }

            _isDying = true;
        }

        public void Died()
        {
            _isDying = false;
            Destroy(gameObject);
        }
    }
}