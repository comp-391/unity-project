using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearScript : MonoBehaviour
{
    public float moveSpeed = 1f;
    private Rigidbody2D myBody;
    private Animator anim;

    private bool moveRignt;

    public Transform down_Collision;
    void Awake() {
        myBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {
        moveRignt = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (moveRignt) {
            myBody.velocity = new Vector2(moveSpeed, myBody.velocity.y);
        } else {
            myBody.velocity = new Vector2(-moveSpeed, myBody.velocity.y);
        }

        CheckCollision();
    }

    void CheckCollision() {
        if(!Physics2D.Raycast (down_Collision.position, Vector2.down, 0.5f))
        {
            Debug.Log("Change Direction");
            ChangeDirection();
        }
    }

    void ChangeDirection() {
        moveRignt = !moveRignt;
        Vector3 tempScale = transform.localScale;
        tempScale.x *= -1.0f;
        transform.localScale = tempScale;
    }
}
