﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class RespawnUI : MonoBehaviour
    {
        public void Respawn()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}