﻿using System;
using System.Numerics;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;

namespace Platforms
{
    public class PlatformMovement : MonoBehaviour
    {
        [SerializeField] private Vector2 finalPosition;

        private Vector2 _initialPosition;

        private float _timeElapsed = 0.0f;

        private bool _bIsGoingForwards = true;

        [SerializeField]
        private float timeSpan = 5.0f;

        private float distanceNormalizer;
        
        private void Start()
        {
            _initialPosition = transform.position;
            finalPosition += _initialPosition;
            distanceNormalizer = Vector2.Distance(_initialPosition, finalPosition) / timeSpan;
        }

        private void Update()
        {
            if (_bIsGoingForwards)
            {
                _timeElapsed += (Time.deltaTime * distanceNormalizer);
            }
            else
            {
                _timeElapsed -= (Time.deltaTime * distanceNormalizer);
            }
            _timeElapsed = Mathf.Clamp(_timeElapsed, 0.0f, 1.0f);
            transform.Translate(Vector2.Lerp(_initialPosition, finalPosition, _timeElapsed));
            if (_timeElapsed >= 1.0f)
            {
                _bIsGoingForwards = false;
                return;
            }

            if (_timeElapsed <= 0.0f)
            {
                _bIsGoingForwards = true;
            }
        }
    }
}