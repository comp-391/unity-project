﻿using System;
using Cinemachine;
using UnityEngine;
using UnityEngine.Assertions;

namespace Player
{
    public class PlayerSpawner : MonoBehaviour
    {
        [SerializeField]
        protected GameObject cinemachineGameObject;

        private CinemachineVirtualCamera _virtualCamera;

        [SerializeField]
        protected GameObject playerGameObject;

        private GameObject _currentPlayer = null;

        private void Start()
        {
            if (cinemachineGameObject == null) return;
            _virtualCamera = cinemachineGameObject.GetComponent<CinemachineVirtualCamera>();
            Assert.IsFalse(_virtualCamera == null);
        }

        public void Spawn()
        {
            if (_currentPlayer == null)
            {
                _currentPlayer = Instantiate(playerGameObject, transform);
                _virtualCamera.Follow = _currentPlayer.transform;
                return;
            }

            _currentPlayer.transform.position = transform.position;
            _currentPlayer.transform.rotation = transform.rotation;
            _virtualCamera.Follow = _currentPlayer.transform;
        }
    }
}