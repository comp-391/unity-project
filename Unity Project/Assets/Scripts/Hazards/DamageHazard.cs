﻿using System;
using Characters;
using UnityEngine;

namespace Hazards
{
    public class DamageHazard : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D col)
        {
            col.gameObject.GetComponent<CharacterHealth>()?.ReceiveHit();
        }
    }
}