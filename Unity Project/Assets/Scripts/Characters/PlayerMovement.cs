using System;
using System.Collections;
using System.Collections.Generic;
using Characters.Interfaces;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;


namespace Characters
{
    public class PlayerMovement : MonoBehaviour, IMovementComponentInterface, IRespawnInterface
    {
        private bool isDead = false;
        
        [SerializeField] private Rigidbody2D playerRigidBody;

        [SerializeField] private float playerSpeed = 50.0f;

        [SerializeField] private float gradientSpeed = 1.0f;

        public float PlayerSpeed => playerSpeed;

        private float _desiredSpeed = 0.0f;

        private float _currentTimeCounter = 0.0f;

        [SerializeField] private bool isCharacterLookingRight = true;
        
        private bool _isInGround;
            
        private OnDirectionChanged _onDirectionChangedDelegate;

        private OnWantsToMove _onWantsToMove;

        private OnJumped _onJumped;

        private OnGrounded _onGrounded;
        
        [SerializeField] private float jumpForceMultiplier = 10.0f;

        // Start is called before the first frame update
        private void Start()
        {
            Debug.Assert(playerRigidBody != null);
        }

        public void Move(InputAction.CallbackContext context)
        {
            if (isDead) return;
            float contextDirection = context.ReadValue<Vector2>().x;
            ChangeDirection(contextDirection);
            _desiredSpeed = contextDirection * playerSpeed;
            _onWantsToMove.Invoke(_desiredSpeed);
            if (!Mathf.Approximately(_desiredSpeed, 0.0f))
            {
                return;
            }

            _currentTimeCounter = 0.0f;
        }

        public void Jump(InputAction.CallbackContext context)
        {
            if (isDead) return;
            if (!_isInGround) return;
            Vector2 force = Vector2.up * 0.5f * jumpForceMultiplier;
            playerRigidBody.AddForce(force, ForceMode2D.Impulse);
            _onJumped.Invoke();
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (!col.gameObject.CompareTag("Ground")) return;
            _isInGround = true;
            _onGrounded.Invoke();
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (!other.gameObject.CompareTag("Ground")) return;
            _isInGround = false;
        }

        private void Update()
        {
            UpdateMovement();
        }

        private void UpdateMovement()
        {
            Vector2 playerVelocity = playerRigidBody.velocity;
            if (Mathf.Approximately(_desiredSpeed, 0.0f))
            {
                Mathf.Lerp(playerVelocity.x, 0.0f, _currentTimeCounter);
                _currentTimeCounter += Time.deltaTime * gradientSpeed;
                return;
            }

            float xVelocity = (_desiredSpeed * (Time.deltaTime * gradientSpeed)) + playerVelocity.x;
            xVelocity = Mathf.Clamp(xVelocity, -playerSpeed, playerSpeed);
            playerRigidBody.velocity = new Vector2(xVelocity, playerRigidBody.velocity.y);
        }

        private void ChangeDirection(float contextDirection)
        {
            if (isCharacterLookingRight && contextDirection < 0.0f)
            {
                isCharacterLookingRight = false;
                _onDirectionChangedDelegate.Invoke(isCharacterLookingRight);
                return;
            }

            if (!isCharacterLookingRight && contextDirection > 0.0f)
            {
                isCharacterLookingRight = true;
                _onDirectionChangedDelegate.Invoke(isCharacterLookingRight);
            }
        }

        public void BindToDirectionChanged(OnDirectionChanged bindingDelegate)
        {
            _onDirectionChangedDelegate += bindingDelegate;
        }

        public void UnBindToDirectionChanged(OnDirectionChanged bindingDelegate)
        {
            _onDirectionChangedDelegate -= bindingDelegate;
        }

        public void BindToTryToMove(OnWantsToMove bindingDelegate)
        {
            _onWantsToMove += bindingDelegate;
        }

        public void UnBindToTryToMove(OnWantsToMove bindingDelegate)
        {
            _onWantsToMove -= bindingDelegate;
        }

        public void BindToOnJumped(OnJumped bindingDelegate)
        {
            _onJumped += bindingDelegate;
        }

        public void UnBindToOnJumped(OnJumped bindingDelegate)
        {
            _onJumped -= bindingDelegate;
        }

        public void BindToOnGrounded(OnGrounded bindingDelegate)
        {
            _onGrounded += bindingDelegate;
        }

        public void UnBindToOnGrounded(OnGrounded bindingDelegate)
        {
            _onGrounded -= bindingDelegate;
        }

        public float GetMovementSpeed()
        {
            return playerRigidBody.velocity.x;
        }

        public void OnDied()
        {
            isDead = true;
        }

        public void Respawned()
        {
            isDead = false;
        }
    }
}