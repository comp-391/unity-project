﻿using System;
using System.Collections;
using System.Collections.Generic;
using Characters.Interfaces;
using UnityEngine;

namespace Characters
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class SpriteAnimationController : MonoBehaviour, IRespawnInterface
    {
        [SerializeField] private SpriteRenderer gameObjectSpriteRenderer;

        [SerializeField] private Animator animator;

        [SerializeField] private Rigidbody2D characterRigidbody2D;

        private IMovementComponentInterface _movementComponent;
        
        private static readonly int SpeedAnimator = Animator.StringToHash("Speed");
        private static readonly int HasJumped = Animator.StringToHash("hasJumped");
        private static readonly int IsGrounded = Animator.StringToHash("isInGround");
        private static readonly int IsDead = Animator.StringToHash("isDead");

        private event OnDirectionChanged _onDirectionChanged;

        private event OnWantsToMove OnWantingToMove;

        private event OnJumped OnJump;

        private event OnGrounded OnGround;

        public SpriteAnimationController()
        {
            _onDirectionChanged = OnDirectionChange;
            OnWantingToMove = OnTryToMove;
            OnJump = OnJumped;
            OnGround = OnGrounded;
        }
        
        private void Start()
        {
            if (animator == null)
            {
                animator = GetComponent<Animator>();
            }

            if (characterRigidbody2D == null)
            {
                characterRigidbody2D = GetComponent<Rigidbody2D>();
            }
            if (gameObjectSpriteRenderer == null)
            {
                gameObjectSpriteRenderer = GetComponent<SpriteRenderer>();
            }
            _movementComponent = GetComponent<IMovementComponentInterface>();
            Debug.Assert(_movementComponent != null);
            _movementComponent?.BindToDirectionChanged(_onDirectionChanged);
            _movementComponent?.BindToTryToMove(OnWantingToMove);
            _movementComponent?.BindToOnJumped(OnJump);
            _movementComponent?.BindToOnGrounded(OnGround);
        }

        private void OnDestroy()
        {
            _movementComponent?.UnBindToDirectionChanged(_onDirectionChanged);
            _movementComponent?.UnBindToTryToMove(OnWantingToMove);
            _movementComponent?.UnBindToOnJumped(OnJump);
            _movementComponent?.UnBindToOnGrounded(OnGround);
        }

        private void OnDirectionChange(bool isLookingRight)
        {
            gameObjectSpriteRenderer.flipX = !isLookingRight;
        }

        private void OnTryToMove(float desiredSpeed)
        {
            if (animator == null) return;
            animator.SetFloat(SpeedAnimator, Math.Abs(desiredSpeed));
        }

        private void OnJumped()
        {
            if (animator == null) return;
            animator.SetBool(HasJumped, true);
            animator.SetBool(IsGrounded, false);
        }

        private void OnGrounded()
        {
            if (animator == null) return;
            animator.SetBool(HasJumped, false);
            animator.SetBool(IsGrounded, true);
        }

        public void OnDied()
        {
            if (animator == null) return;
            animator.SetBool(IsDead, true);
        }

        private void Update()
        {
        }

        public void Respawned()
        {
            if (animator == null) return;
            animator.SetBool(IsDead, false);
            animator.StartPlayback();
        }
    }
}