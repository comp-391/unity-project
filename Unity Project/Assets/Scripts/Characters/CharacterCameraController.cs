﻿using System;
using Cinemachine;
using UnityEngine;

namespace Characters
{
    public class CharacterCameraController : MonoBehaviour
    {
        [SerializeField] private GameObject cinemachineFollowCamera;

        private CinemachineVirtualCamera _virtualCamera;

        void SetFollowCamera(GameObject newFollowCamera)
        {
            cinemachineFollowCamera = newFollowCamera;
            _virtualCamera = cinemachineFollowCamera.GetComponent<CinemachineVirtualCamera>();
        }
        
        private void Start()
        {
            _virtualCamera = cinemachineFollowCamera.GetComponent<CinemachineVirtualCamera>();
        }

        public void UnFollow()
        {
            if (_virtualCamera == null) return;
            _virtualCamera.Follow = null;
        }
    }
}