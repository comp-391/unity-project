﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Characters
{
    public class DeathComponent : MonoBehaviour
    {

        private bool hasDied = false;
        
        private void Start()
        {
            
        }

        public void OnDied()
        {
            if (hasDied) return;
            SceneManager.LoadScene("RespawnUI", LoadSceneMode.Additive);
            hasDied = true;
        }
    }
}