﻿using System;
using Unity.VisualScripting;


namespace Characters.Interfaces
{
    public delegate void OnDirectionChanged(bool isLookingRight);

    public delegate void OnWantsToMove(float desiredSpeed);

    public delegate void OnJumped();

    public delegate void OnGrounded();
    public interface IMovementComponentInterface
    {
        void BindToDirectionChanged(OnDirectionChanged bindingDelegate);

        void UnBindToDirectionChanged(OnDirectionChanged bindingDelegate);

        void BindToTryToMove(OnWantsToMove bindingDelegate);

        void UnBindToTryToMove(OnWantsToMove bindingDelegate);

        void BindToOnJumped(OnJumped bindingDelegate);
        
        void UnBindToOnJumped(OnJumped bindingDelegate);

        void BindToOnGrounded(OnGrounded bindingDelegate);
        
        void UnBindToOnGrounded(OnGrounded bindingDelegate);
        
        float GetMovementSpeed();
    }
}