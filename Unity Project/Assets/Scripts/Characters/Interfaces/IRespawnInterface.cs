﻿namespace Characters.Interfaces
{
    public interface IRespawnInterface
    {
        abstract void Respawned();
    }
}