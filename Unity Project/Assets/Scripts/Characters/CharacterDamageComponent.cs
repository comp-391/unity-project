﻿using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Characters
{
    public class CharacterDamageComponent : MonoBehaviour
    {
        private void Start()
        {
            
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (!col.gameObject.CompareTag("Enemy")) return;
            CharacterHealth characterHealth = col.GetComponent<CharacterHealth>();
            if (characterHealth == null) return;
            characterHealth.ReceiveHit();
        }

    }
}