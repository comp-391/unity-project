﻿using System;
using Characters.Interfaces;
using UnityEngine;
using UnityEngine.Events;

namespace Characters
{
    public class CharacterHealth : MonoBehaviour, IRespawnInterface
    {
        [SerializeField] protected float initialHealth = 10;

        private float _currentHealth;

        [SerializeField]
        public UnityEvent onDied;

        private void Start()
        {
            _currentHealth = initialHealth;
        }

        public virtual void ReceiveHit()
        {
            _currentHealth -= initialHealth;
            if (_currentHealth <= 0.0f)
            {
                onDied.Invoke();
            }
        }

        public virtual void RestartHealth()
        {
            _currentHealth = initialHealth;
        }

        public void Respawned()
        {
            RestartHealth();
        }
    }
}